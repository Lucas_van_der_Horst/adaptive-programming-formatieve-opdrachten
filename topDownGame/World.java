package topDownGame;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class World {
    private Tile grid[][];
    private Camera camera;
    private ArrayList<int[]> movable_locations = new ArrayList<int[]>();   // the location in the grid of movable things, aka players and enemies

    public World(int width, int height, int[] spawn_location, int[] screen_size, int space_density){
        this.grid = new Tile[height][width];
        this.camera = new Camera(spawn_location, screen_size);
        addCharacter(new Player(), spawn_location);
        this.generate_world(space_density);
    }

    private int[] empty_location(){
        ThreadLocalRandom rand = ThreadLocalRandom.current();
        int x = 0;
        int y = 0;
        while(getTile(new int[]{x, y}) != null){
            x = rand.nextInt(0, this.grid[0].length);
            y = rand.nextInt(0, this.grid.length);
        }
        return new int[]{x, y};
    }

    private void generate_world(int space_density){
        // soft locking is possible with this method :(
        for(int i=0; i<surface_size()/space_density; i++){
            int[] location = empty_location();
            this.grid[location[1]][location[0]] = new Tile("w");
        }
        for(int i=0; i<8; i++){
            addCharacter(new Enemy(), empty_location());
        }
    }

    public void addCharacter(Character character, int[] location){
        character.setLocation(location);
        this.grid[location[1]][location[0]] = character;
        this.movable_locations.add(location);
    }

    public void move_everything(){
        ArrayList<int[]> old_locations = new ArrayList<int[]>(this.movable_locations);
        this.movable_locations = new ArrayList<int[]>();
        for(int[] moveable_location : old_locations){
            Tile tile = getTile(moveable_location);
            this.grid[moveable_location[1]][moveable_location[0]] = null;
            Character moved = tile.move(this);
            addCharacter(moved, moved.location);
        }
        this.camera.setLocation(this.movable_locations.get(0));
    }

    public Tile getTile(int[] location){
        int x = location[0];
        int y = location[1];
        if(x>=0 && x<this.grid[0].length && y>=0 && y<this.grid.length){
            return this.grid[y][x];
        } else {
            return new Tile("#");
        }
    }

    public ArrayList<Tile> getPlayers(){
        ArrayList<Tile> players = new ArrayList<Tile>();
        for(int[] moveable_location : this.movable_locations){
            Tile tile = getTile(moveable_location);
            if(Player.class == tile.getClass()){
                players.add(tile);
            }
        }
        return players;
    }

    public void display(){
        int[] camera_location = this.camera.getLocation();
        int[] camera_screensize = this.camera.getScreen_size();
        for(int y=0; y<camera_screensize[1]; y++){
            for(int x=0; x<camera_screensize[0]; x++){
                int[] pixel_location = new int[]{x+camera_location[0]-camera_screensize[0]/2, y+camera_location[1]-camera_screensize[1]/2};
                Tile tile = this.getTile(pixel_location);
                String symbol;
                if(tile == null){
                    symbol = " ";
                } else {
                    symbol = tile.toString();
                }
                System.out.print(symbol + "");
            }
            System.out.println();
        }
    }

    public int surface_size() {return this.grid.length * this.grid[0].length;}

    public boolean check_death(){
        ArrayList<Tile>  players = getPlayers();
        for(Tile player : players){
            int[] location = player.getLocation();
            // check if any of the serounding tiles are enemies
            if(Enemy.class == replace_if_null(getTile(new int[]{location[0]+1, location[1]})).getClass()
            || Enemy.class == replace_if_null(getTile(new int[]{location[0]-1, location[1]})).getClass()
            || Enemy.class == replace_if_null(getTile(new int[]{location[0], location[1]+1})).getClass()
            || Enemy.class == replace_if_null(getTile(new int[]{location[0], location[1]-1})).getClass()){
                return true;
            }
        }
        return false;
    }

    public Tile replace_if_null(Tile check){
        if(check == null){
            return new Tile("");
        } else {
            return check;
        }
    }
}
