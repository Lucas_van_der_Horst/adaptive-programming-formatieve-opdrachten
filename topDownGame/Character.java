package topDownGame;

// Interface
public class Character extends Tile {
    int[] location;

    public Character(String symbol) {
        super(symbol);
    }

    public void setLocation(int[] location) {this.location = location;}

    @Override
    public int[] getLocation(){
        return this.location;
    }

    public boolean check_move(World world, int[] location) {
        return world.getTile(location) == null;
    }
}
