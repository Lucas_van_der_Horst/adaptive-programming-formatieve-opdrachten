package topDownGame;

// Interface
public class Tile {
    String symbol;

    public Tile(String symbol) {
        this.symbol = symbol;
    }

    public Character move(World world){
        return new Character("error");
    }

    public int[] getLocation() {return new int[0];}

    @Override
    public String toString() {return this.symbol;}
}
