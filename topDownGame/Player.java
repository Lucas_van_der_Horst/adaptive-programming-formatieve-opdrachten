package topDownGame;
import java.util.Arrays;
import java.util.Scanner;

public class Player extends Character {
    public Player() {
        super("\033[92mP\033[0m");
    }

    @Override
    public Character move(World world){
        Scanner scanner = new Scanner(System.in);
        boolean getting_input = true;
        while(getting_input){
            System.out.println("Where do you want to move? options:");
            System.out.println("North/Up, East/Right, South/Down or West/Left? Type below\t(first character works too)");
            String user_input = scanner.nextLine().toLowerCase();   //Get user input
            int[] try_location;
            if(Arrays.asList(new String[]{"west", "w", "left", "l"}).contains(user_input)) {
                try_location = new int[]{this.location[0]-1, this.location[1]};
            } else if(Arrays.asList(new String[]{"east", "e", "right", "r"}).contains(user_input)) {
                try_location = new int[]{this.location[0]+1, this.location[1]};
            } else if(Arrays.asList(new String[]{"north", "n", "up", "u"}).contains(user_input)) {
                try_location = new int[]{this.location[0], this.location[1]-1};
            } else if(Arrays.asList(new String[]{"south", "s", "down", "d"}).contains(user_input)) {
                try_location = new int[]{this.location[0], this.location[1]+1};
            } else {
                try_location = this.location;
            }
            if(check_move(world, try_location)){
                this.location = try_location;
                getting_input = false;
            } else {
                System.out.println("Obstacle in the way");
            }
        }
        return this;
    }
}
