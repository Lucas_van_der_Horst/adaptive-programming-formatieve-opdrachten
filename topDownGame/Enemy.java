package topDownGame;

import java.util.ArrayList;
import java.util.Arrays;

public class Enemy extends Character {

    public Enemy() {
        super("\033[31me\033[0m");
    }

    @Override
    public Character move(World world){     // A simple ai to follow the player
        ArrayList<Tile> players = world.getPlayers();
        int[] focus_location = players.get(0).getLocation();    // focus on the first player
        int[] difference =  new int[]{  // distance to the player in the four directions
                focus_location[0] - this.location[0],   // Right
                this.location[0] - focus_location[0],   // Left
                focus_location[1] - this.location[1],   // Down
                this.location[1] - focus_location[1],   // Up
        };

        // don't collide with obstacle
        if(world.getTile(new int[]{this.location[0]+1, this.location[1]}) != null){
            difference[0] = -9999;
        }
        if(world.getTile(new int[]{this.location[0]-1, this.location[1]}) != null){
            difference[1] = -9999;
        }
        if(world.getTile(new int[]{this.location[0], this.location[1]+1}) != null){
            difference[2] = -9999;
        }
        if(world.getTile(new int[]{this.location[0], this.location[1]-1}) != null){
            difference[3] = -9999;
        }

        int largest_distance_index = getArrayIndex(difference, Arrays.stream(difference).max().getAsInt());//which direction
        if(largest_distance_index == 0){
            this.location[0] += 1;
        } else if(largest_distance_index == 1) {
            this.location[0] -= 1;
        } else if(largest_distance_index == 2) {
            this.location[1] += 1;
        } else if(largest_distance_index == 3) {
            this.location[1] -= 1;
        }
        return this;
    }

    public int getArrayIndex(int[] arr, int value) { // find index of value in array
        int k=0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]==value){
                k=i;
                break;
            }
        }
        return k;
    }
}
