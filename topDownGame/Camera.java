package topDownGame;

public class Camera {
    private int[] location;
    private int[] screen_size;

    public Camera(int[] location, int[] screen_size){
        this.setLocation(location);
        this.screen_size = screen_size;
    }

    public void setLocation(int[] location) {this.location = location;}

    public int[] getLocation() {return this.location;}

    public int[] getScreen_size() {return this.screen_size;}
}
