package topDownGame;

public class main {
    public static void main(String[] args){
        System.out.println("A simple top down game.");
        System.out.println("Objective: you the \033[92mp\033[0mlayer run away from the \033[31me\033[0mnemies while avoiding the w(alls).");
        System.out.println("The camera will follow the player.");
        System.out.println();

        int[] spawn_location = {4, 3};
        int[] screen_size = {30, 10};
        int world_width = 40;
        int world_heigth = 20;
        World world = new World(world_width, world_heigth, spawn_location, screen_size, 3);

        boolean running = true;
        while(!world.check_death()){
            world.display();
            world.move_everything();
        }

        world.display();
        System.out.println("\nGAME OVER");
    }
}
