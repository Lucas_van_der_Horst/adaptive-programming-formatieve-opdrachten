package MonteCarloMachine;

public class Main {
    public static void main(String[] args){
        six_sided_dice();
        eight_sided_dice();
    }

    private static void six_sided_dice() {
        Node s0 = new Node("s0");
        Node s1 = new Node("s1");
        Node s2 = new Node("s2");
        Node s3 = new Node("s3");
        Node s4 = new Node("s4");
        Node s5 = new Node("s5");
        Node s6 = new Node("s6");
        Node dice1 = new Node("1");
        Node dice2 = new Node("2");
        Node dice3 = new Node("3");
        Node dice4 = new Node("4");
        Node dice5 = new Node("5");
        Node dice6 = new Node("6");

        s0.add_connection("A", s1);
        s0.add_connection("B", s2);

        s1.add_connection("A", s3);
        s1.add_connection("B", s4);

        s2.add_connection("A", s5);
        s2.add_connection("B", s6);

        s3.add_connection("A", s1);
        s3.add_connection("B", dice1);

        s4.add_connection("A", dice2);
        s4.add_connection("B", dice3);

        s5.add_connection("A", dice4);
        s5.add_connection("B", dice5);

        s6.add_connection("A", dice6);
        s6.add_connection("B", s2);

        Machine monte_carlo_machine = new Machine();
        System.out.println("Six sided dice throw: " + monte_carlo_machine.throw_dice(s0));
    }

    private static void eight_sided_dice() {
        Node s0 = new Node("s0");
        Node s1 = new Node("s1");
        Node s2 = new Node("s2");
        Node s3 = new Node("s3");
        Node s4 = new Node("s4");
        Node s5 = new Node("s5");
        Node s6 = new Node("s6");
        Node dice1 = new Node("1");
        Node dice2 = new Node("2");
        Node dice3 = new Node("3");
        Node dice4 = new Node("4");
        Node dice5 = new Node("5");
        Node dice6 = new Node("6");
        Node dice7 = new Node("7");
        Node dice8 = new Node("8");

        s0.add_connection("A", s1);
        s0.add_connection("B", s2);

        s1.add_connection("A", s3);
        s1.add_connection("B", s4);

        s2.add_connection("A", s5);
        s2.add_connection("B", s6);

        s3.add_connection("A", dice1);
        s3.add_connection("B", dice2);

        s4.add_connection("A", dice3);
        s4.add_connection("B", dice4);

        s5.add_connection("A", dice5);
        s5.add_connection("B", dice6);

        s6.add_connection("A", dice7);
        s6.add_connection("B", dice8);

        Machine monte_carlo_machine = new Machine();
        System.out.println("Eight sided dice throw: " + monte_carlo_machine.throw_dice(s0));
    }
}
