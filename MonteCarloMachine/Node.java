package MonteCarloMachine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Node {
    private final String name;
    private final Map<String, Node> connected = new HashMap<>();

    public Node(String name){
        this.name = name;
    }

    public void add_connection(String pathname, Node node){
        this.connected.put(pathname, node);
    }

    public Node get_next_node(String path_name){
        if(connected.containsKey(path_name)){
            return connected.get(path_name);
        } else {
            throw new IllegalArgumentException(path_name + " is not a path of node " + name);
        }
    }

    public boolean is_end_node(){
        return connected.size() == 0;
    }

    public String get_name(){
        return name;
    }

    public String random_path(){
        Random generator = new Random();
        return (String)connected.keySet().toArray()[generator.nextInt(connected.size())];
    }

    // remainder of "Machine met tekstinvoer"
    public ArrayList<String> run(String input_text){
        if(input_text.length() == 0){
            // end of recursion
            // if the input_text is empty, return a arraylist with only the name of the node
            return new ArrayList<>() {{
                add(name);
            }};
        } else {
            String key = input_text.substring(0, 1);    // first letter in string
            String remainder = input_text.substring(1); // rest of the string

            if(connected.containsKey(key)){
                // if path exists, use recursion to run the connected node
                ArrayList<String> result = connected.get(key).run(remainder);
                // after the "sub-node" is done, add name to the front of the result
                result.add(0, name);
                return result;
            } else {
                throw new IllegalArgumentException(key + " is not a path of node " + name);
            }
        }
    }
    /*
    Sources:
    https://stackoverflow.com/questions/929554/is-there-a-way-to-get-the-value-of-a-hashmap-randomly-in-java
     */
}
