package MonteCarloMachine;

public class Machine {
    public String throw_dice(Node begin_node){
        Node working_node = begin_node;
        while(!working_node.is_end_node()){
            String path = working_node.random_path();
            try {
                working_node = working_node.get_next_node(path);
            } catch (IllegalArgumentException ex){
                return "an error: " + ex.getMessage();
            }
        }
        return working_node.get_name();
    }
}
