package gameshop;

public class Game {
    private String naam;
    private int releaseJaar;
    private double releasePrice;

    public Game(String nm, int rJ, double rP){
        this.naam = nm;
        this.releaseJaar = rJ;
        this.releasePrice = rP;
    }

    public String getNaam(){
        return this.naam;
    }

    public int getReleaseJaar(){
        return this.releaseJaar;
    }

    public double getReleasePrice(){
        return this.releasePrice;
    }

    public double currentPrice(int year){
        double price = this.getReleasePrice();
        for(int i = 0; i < year-this.getReleaseJaar(); i++){
            price = price * 0.7;
        }
        return Math.round(price * 100.0) / 100.0;
    }

    public String toString(int year){
        return this.getNaam() + ", uitgegeven in " + this.getReleaseJaar() + "; " + this.getReleasePrice() + " nu voor: "+ this.currentPrice(year);
    }
}
