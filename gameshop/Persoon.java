package gameshop;

import java.time.LocalDate;
import java.util.ArrayList;

public class Persoon {
    private String naam;
    private int budget;
    private ArrayList<Game> bezit = new ArrayList<Game>();

    public Persoon(String naam, int budget){
        this.naam = naam;
        this.budget =budget;
    }

    public String getNaam(){
        return this.naam;
    }

    public void addGame(Game game){
        this.bezit.add(game);
    }

    private void removeGame(Game game){
        this.bezit.remove(game);
    }

    public void addBudget(double price){
        this.budget += price;
    }

    public boolean gameInBudget(Game game, int currentYear){
        double price = game.currentPrice(currentYear);
        return price <= this.budget;
    }

    private boolean gameInBezit(Game game){
        return this.bezit.contains(game);
    }

    public boolean tryKoop(Game game, int currentYear){
        if(this.gameInBudget(game, currentYear)){
            this.budget -= game.currentPrice(currentYear);
            this.addGame(game);
            return true;
        }else{
            return false;
        }
    }

    public void koop(Game game, int currentYear){
        System.out.print(this.getNaam() + " koopt " + game.getNaam() + ": ");
        if(!this.tryKoop(game, currentYear)){
            System.out.print("niet ");
        }
        System.out.println("gelukt");
    }

    public boolean tryVerkoop(Game game, Persoon buyer, int currentYear){
        if(buyer.gameInBudget(game, currentYear) && this.gameInBezit(game)){
            double price = game.currentPrice(currentYear);
            this.removeGame(game);
            this.addBudget(price);
            buyer.tryKoop(game, currentYear);
            return true;
        }else{
            return false;
        }
    }

    public void verkoop(Game game, Persoon buyer, int currentYear){
        System.out.print(this.getNaam() + " verkoopt " + game.getNaam() + " aan " + buyer.getNaam() + ": ");
        if(!this.tryVerkoop(game, buyer, currentYear)){
            System.out.print("niet ");
        }
        System.out.println("gelukt");
    }

    public void printStatus(int year){
        System.out.println("");
        System.out.println(this.getNaam() + " heeft een budget van €" + this.budget + " en bezit de volgende games:");
        for (Game game : this.bezit) {
            System.out.println(game.toString(year));
        }
        System.out.println("");
    }
}
