package FiniteStateMachine_Testing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Node {
    private final String name;
    private final Map<String, Node> connected = new HashMap<>();
    private final ArrayList<Double> chances = new ArrayList<>(); //values between 0.0 and 1.0

    public Node(String name){
        this.name = name;
    }

    public void add_connection(String pathname, Node node, double chance){
        this.connected.put(pathname, node);
        this.chances.add(chance);
    }

    public Node get_node_by_path(String path_name){
        if(connected.containsKey(path_name)){
            return connected.get(path_name);
        } else {
            throw new IllegalArgumentException(path_name + " is not a path of node " + name);
        }
    }

    public ArrayList<Double> get_chance_distribution(){
        return chances;
    }

    public String location_to_path(int location){
        ArrayList<String> as_list = new ArrayList<>(connected.keySet());
        if(location < as_list.size()){
            return as_list.get(location);
        } else {
            throw new IllegalArgumentException("this node doesn't have " + (location+1) + " paths.");
        }
    }

    public boolean is_end_node(){
        return connected.size() == 0;
    }

    public String get_name(){
        return name;
    }

    @Override
    public String toString(){
        return get_name();
    }
}
