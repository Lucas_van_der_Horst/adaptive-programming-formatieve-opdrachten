package FiniteStateMachine_Testing;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyUnitTest {

    @Test
    public void diamond_structure(Machine machine){
        Node s0 = new Node("s0");
        Node s1 = new Node("s1");
        Node s2 = new Node("s2");
        Node s3 = new Node("s3");

        s0.add_connection("A", s2, 0.5);
        s0.add_connection("B", s1, 0.5);

        s1.add_connection("A", s1, 0.5);
        s1.add_connection("B", s2, 0.5);

        s2.add_connection("B", s3, 1.0);

        s3.add_connection("A", s3, 0.5);
        s3.add_connection("B", s0, 0.5);

        text_correct_basis(machine, s0, s1, s2, s3);
        text_wrong(machine, s0, s1, s2, s3);
        text_correct_long(machine, s0, s1, s2, s3);

        System.out.println("Completed diamond structure tests\n");
    }

    @Test
    public void text_correct_basis(Machine machine, Node s0, Node s1, Node s2, Node s3){
        try{
            ArrayList<Node> result = machine.run(s0, new ArrayList<>() {{
                add("B");
                add("A");
                add("A");
                add("B");
                add("stop");
            }});

            ArrayList<Node> expected = new ArrayList<>() {{
                add(s0);
                add(s1);
                add(s1);
                add(s1);
                add(s2);
            }};

            assertEquals(expected, result);
            System.out.println("Successful text_correct_basis, result: " + result);
        } catch (Exception e){
            System.out.println("Error in text_correct_basis: " + e);
        }
    }

    @Test
    public void text_wrong(Machine machine, Node s0, Node s1, Node s2, Node s3){
        try{
            ArrayList<Node> result = machine.run(s0, new ArrayList<>() {{
                add("B");
                add("A");
                add("B");
                add("A");
                add("stop");
            }});

            ArrayList<Node> expected = new ArrayList<>() {{
                add(s0);
                add(s1);
                add(s1);
                add(s1);
                add(s2);
            }};

            assertEquals(expected, result);
            System.out.println("Successful text_wrong, result: " + result);
        } catch (Exception e){
            System.out.println("Error in text_wrong: " + e);
        }
    }

    @Test
    public void text_correct_long(Machine machine, Node s0, Node s1, Node s2, Node s3){
        try{
            ArrayList<Node> result = machine.run(s2, new ArrayList<>() {{
                add("B");
                add("A");
                add("B");
                add("A");
                add("B");
                add("B");
                add("B");
                add("A");
                add("B");
                add("stop");
            }});

            ArrayList<Node> expected = new ArrayList<>() {{
                add(s2);
                add(s3);
                add(s3);
                add(s0);
                add(s2);
                add(s3);
                add(s0);
                add(s1);
                add(s1);
                add(s2);
            }};

            assertEquals(expected, result);
            System.out.println("Successful text_correct_long, result: " + result);
        } catch (Exception e){
            System.out.println("Error in text_correct_long: " + e);
        }
    }

    @Test
    public void six_sided_dice_structure(Machine machine){
        Node s0 = new Node("s0");
        Node s1 = new Node("s1");
        Node s2 = new Node("s2");
        Node s3 = new Node("s3");
        Node s4 = new Node("s4");
        Node s5 = new Node("s5");
        Node s6 = new Node("s6");
        Node dice1 = new Node("1");
        Node dice2 = new Node("2");
        Node dice3 = new Node("3");
        Node dice4 = new Node("4");
        Node dice5 = new Node("5");
        Node dice6 = new Node("6");

        s0.add_connection("A", s1, 0.5);
        s0.add_connection("B", s2, 0.5);

        s1.add_connection("A", s3, 0.5);
        s1.add_connection("B", s4, 0.5);

        s2.add_connection("A", s5, 0.5);
        s2.add_connection("B", s6, 0.5);

        s3.add_connection("A", s1, 0.5);
        s3.add_connection("B", dice1, 0.5);

        s4.add_connection("A", dice2, 0.5);
        s4.add_connection("B", dice3, 0.5);

        s5.add_connection("A", dice4, 0.5);
        s5.add_connection("B", dice5, 0.5);

        s6.add_connection("A", dice6, 0.5);
        s6.add_connection("B", s2, 0.5);

        ArrayList<Node> output = machine.run(s0, new ArrayList<>() {{
            add("random from now on");
        }});
        System.out.println("Six sided dice roll: " + output.get(output.size() - 1));

        output = machine.run(s0, new ArrayList<>() {{
            add("A");
            add("random from now on");
        }});
        System.out.println("Six sided dice roll (only 1, 2 or 3 possible): " + output.get(output.size() - 1));

        System.out.println("Completed six_sided_dice structure tests\n");
    }

    @Test
    public void eight_sided_dice_structure(Machine machine){
        Node s0 = new Node("s0");
        Node s1 = new Node("s1");
        Node s2 = new Node("s2");
        Node s3 = new Node("s3");
        Node s4 = new Node("s4");
        Node s5 = new Node("s5");
        Node s6 = new Node("s6");
        Node dice1 = new Node("1");
        Node dice2 = new Node("2");
        Node dice3 = new Node("3");
        Node dice4 = new Node("4");
        Node dice5 = new Node("5");
        Node dice6 = new Node("6");
        Node dice7 = new Node("7");
        Node dice8 = new Node("8");

        s0.add_connection("A", s1, 0.5);
        s0.add_connection("B", s2, 0.5);

        s1.add_connection("A", s3, 0.5);
        s1.add_connection("B", s4, 0.5);

        s2.add_connection("A", s5, 0.5);
        s2.add_connection("B", s6, 0.5);

        s3.add_connection("A", dice1, 0.5);
        s3.add_connection("B", dice2, 0.5);

        s4.add_connection("A", dice3, 0.5);
        s4.add_connection("B", dice4, 0.5);

        s5.add_connection("A", dice5, 0.5);
        s5.add_connection("B", dice6, 0.5);

        s6.add_connection("A", dice7, 0.5);
        s6.add_connection("B", dice8, 0.5);

        ArrayList<Node> output = machine.run(s0, new ArrayList<>() {{
            add("random from now on");
        }});
        System.out.println("Eight sided dice roll: " + output.get(output.size() - 1));

        output = machine.run(s0, new ArrayList<>() {{
            add("A");
            add("random from now on");
        }});
        System.out.println("Six sided dice roll (only 1, 2, 3 or 4 possible): " + output.get(output.size() - 1));

        System.out.println("Completed eight_sided_dice structure tests\n");
    }
}
