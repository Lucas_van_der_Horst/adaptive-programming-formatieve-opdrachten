package FiniteStateMachine_Testing;

public class Main {
    public static void main(String[] args){
        Machine machine = new Machine();
        MyUnitTest myUnitTest = new MyUnitTest();
        myUnitTest.diamond_structure(machine);
        myUnitTest.six_sided_dice_structure(machine);
        myUnitTest.eight_sided_dice_structure(machine);
    }
}
