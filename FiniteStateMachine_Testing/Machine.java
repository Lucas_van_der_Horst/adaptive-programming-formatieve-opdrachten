package FiniteStateMachine_Testing;

import java.util.ArrayList;
import java.util.Random;

public class Machine {
    public int random_non_uniform_distribution(ArrayList<Double> chances){
        double sum = 0; for(Double d : chances){sum += d;}
        if(sum > 1.0){throw new java.lang.Error("Chances add up to more than 1.0");}
        Random rand = new Random();
        double chance_left = 1.0;
        for(int i=0; i<chances.size(); i++){
            if(rand.nextDouble() < chances.get(i)/chance_left){
                return i;
            } else {
                chance_left -= chances.get(i);
            }
        }
        return chances.size()-1;
    }

    public ArrayList<Node> run(Node begin_node, ArrayList<String> directions){
        Node working_node = begin_node;
        ArrayList<Node> result = new ArrayList<>();
        result.add(working_node);

        while(!working_node.is_end_node()){
            String next_path;

            if(directions.get(0).equals("stop")){
                break;
            }

            if(!(directions.get(0).equals("random from now on") | directions.get(0).equals("random"))){
                next_path = directions.get(0);
                directions.remove(0);
            } else {
                // random path
                next_path = working_node.location_to_path(random_non_uniform_distribution(working_node.get_chance_distribution()));
                if(directions.get(0).equals("random")){
                    directions.remove(0);
                }
            }

            working_node = working_node.get_node_by_path(next_path);

            result.add(working_node);
        }

        return result;
    }
}
