package DijkstraShortestPath_Datastructuren;

public class Main {
    public static void main(String[] args) {
        Tester tester = new Tester();
        tester.setup();
        tester.test_flight();
        tester.test_drive();

        Visualiser visualisator = new Visualiser(700, 550);
        visualisator.setConnectionsMap(tester.map);
        visualisator.run();
    }
}
