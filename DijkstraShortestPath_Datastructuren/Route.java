package DijkstraShortestPath_Datastructuren;

import java.util.*;

public class Route {
    private final List<Step> steps = new ArrayList<>();

    public void add_step_front(Step step) {
        steps.add(0, step);
    }

    @Override
    public String toString(){
        StringBuilder string_builder = new StringBuilder();
        int cost = 0;
        for(Step step : steps) {
            cost += step.get_cost();
            string_builder.append(step.get_begin_id())
                    .append(" -> ")
                    .append(step.get_end_id())
                    .append("\t| cost: ")
                    .append(cost)
                    .append("\n");
        }
        return string_builder.toString();
    }

    public List<Step> get_steps() {
        return steps;
    }

    public double total_cost() {
        double total_cost = 0;
        for(Step step : get_steps()) {
            total_cost += step.get_cost();
        }
        return total_cost;
    }

    public String get_unit() {
        return get_steps().get(0).get_unit();
    }
}
