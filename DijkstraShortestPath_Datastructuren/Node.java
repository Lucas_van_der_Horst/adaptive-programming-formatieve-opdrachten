package DijkstraShortestPath_Datastructuren;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.awt.geom.Point2D;

import static java.util.stream.Collectors.toList;

public class Node implements Comparable<Node> {
    private final int id;
    private final List<Step> connections_out = new ArrayList<>();
    private final List<Step> connections_in = new ArrayList<>();
    int x;
    int y;
    int size = 50;

    //Only a buffer for Dijkstra's algorithm
    public double cost;
    public Step previous_step;

    public Node(int id, int x, int y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public void add_connection(Step connection) {
        if(connection.get_end_id() == id) {
            connections_in.add(connection);
        }
        if(connection.get_begin_id() == id) {
            this.connections_out.add(connection);
        }
    }

    public int get_id() {
        return id;
    }

    @SuppressWarnings("unchecked")
    public <T> List<Step> get_steps_of_type(Class<T> step_type) {
        //Source: https://stackoverflow.com/a/40989751/12892735
        return (List<Step>) connections_out.stream()
                .filter(step_type::isInstance)
                .map(step_type::cast)
                .collect(toList());
    }

    @Override
    public int compareTo(Node node) {
        //Source: https://stackoverflow.com/a/18355960/12892735
        return Double.compare(cost, node.cost);
    }

    @Override
    public String toString() {
        return "Node " + id;
    }

    void paint(Graphics g, Color color) {
        g.setColor(color);
        g.fillOval(x-size/2, y-size/2, size, size);
        g.setColor(Color.BLACK);
        g.drawOval(x-size/2, y-size/2, size, size);
        g.setFont(new Font("TimesRoman", Font.PLAIN, 20));
        g.drawString(""+id, x-6, y+7);
    }

    void paint_normal(Graphics g) {
        paint(g, Color.WHITE);
    }

    public List<Step> all_connections_in() {
        return connections_in;
    }

    public List<Step> all_connections_out() {
        return connections_out;
    }

    public boolean mouse_on(int mouse_x, int mouse_y) {
        double distance = Point2D.distance(x, y,  mouse_x, mouse_y);
        return distance < size;
    }
}
