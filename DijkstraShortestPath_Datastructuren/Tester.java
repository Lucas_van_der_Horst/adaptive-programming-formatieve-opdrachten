package DijkstraShortestPath_Datastructuren;

import DijkstraShortestPath_Datastructuren.StepTypes.*;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class Tester {
    ConnectionsMap map;

    @BeforeEach
    public void setup() {
        map = new ConnectionsMap();
        map.add_node(new Node(0, 569, 420));
        map.add_node(new Node(1, 80, 400));
        map.add_node(new Node(2, 290, 450));
        map.add_node(new Node(3, 270, 200));
        map.add_node(new Node(4, 580, 180));
        map.add_node(new Node(5, 370, 60));
        map.add_node(new Node(6, 110, 100));

        map.add_node(new Node(7, 450, 640));
        map.add_node(new Node(8, 840, 314));
        map.add_node(new Node(9, 140, 660));
        map.add_node(new Node(10, 740, 610));
        map.add_node(new Node(11, 1020, 730));
        map.add_node(new Node(12, 1010, 100));
        map.add_node(new Node(13, 280, 590));
        map.add_node(new Node(14, 980, 530));
        map.add_node(new Node(15, 40, 560));
        map.add_node(new Node(16, 640, 80));

        map.add_connection_both_ways(new Flight(0, 1, 10.0));
        map.add_connection_both_ways(new Flight(1, 3, 10.0));
        map.add_connection_both_ways(new Flight(0, 2, 5.0));
        map.add_connection_both_ways(new Flight(2, 3, 20.0));
        map.add_connection_both_ways(new Flight(0, 3, 21.0));

        //Example of en.wikipedia.org/wiki/Dijkstra's_algorithm
        map.add_connection_both_ways(new Drive(1, 2, 7.0));
        map.add_connection_both_ways(new Drive(1, 3, 9.0));
        map.add_connection_both_ways(new Drive(1, 6, 14.0));
        map.add_connection_both_ways(new Drive(2, 3, 10.0));
        map.add_connection_both_ways(new Drive(2, 4, 15.0));
        map.add_connection_both_ways(new Drive(3, 4, 11.0));
        map.add_connection_both_ways(new Drive(3, 6, 2.0));
        map.add_connection_both_ways(new Drive(4, 5, 6.0));
        map.add_connection_both_ways(new Drive(6, 5, 9.0));

        map.add_connection_both_ways(new TrainRide(7, 10, 10.0));
        map.add_connection_both_ways(new TrainRide(10, 11, 10.0));
        map.add_connection_both_ways(new TrainRide(10, 14, 10.0));
        map.add_connection_both_ways(new TrainRide(11, 14, 10.0));
        map.add_connection_both_ways(new TrainRide(14, 8, 10.0));
        map.add_connection_both_ways(new TrainRide(8, 16, 10.0));
        map.add_connection_both_ways(new TrainRide(16, 12, 10.0));
        map.add_connection_both_ways(new TrainRide(12, 8, 10.0));
        map.add_connection_both_ways(new TrainRide(13, 7, 10.0));
        map.add_connection_both_ways(new TrainRide(9, 13, 10.0));
        map.add_connection_both_ways(new TrainRide(15, 13, 10.0));
        map.add_connection_both_ways(new TrainRide(15, 9, 10.0));
        map.add_connection_both_ways(new TrainRide(9, 7, 10.0));
        map.add_connection_both_ways(new TrainRide(8, 10, 10.0));
        map.add_connection_both_ways(new TrainRide(7, 0, 10.0));
        map.add_connection_both_ways(new TrainRide(0, 10, 10.0));
        map.add_connection_both_ways(new TrainRide(0, 8, 10.0));
    }

    @Test
    public void test_flight() {
        String expected = "0 -> 1\t| cost: 10\n" + "1 -> 3\t| cost: 20\n";
        Route route = map.dijkstra(0, 3, Flight.class);
        String result = route.toString();
        assertEquals(expected, result);
    }

    @Test
    public void test_drive() {
        String expected = "1 -> 3\t| cost: 9\n" + "3 -> 6\t| cost: 11\n" + "6 -> 5\t| cost: 20\n";
        Route route = map.dijkstra(1, 5, Drive.class);
        String result = route.toString();
        assertEquals(expected, result);
    }
}
