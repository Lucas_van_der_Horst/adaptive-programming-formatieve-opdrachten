package DijkstraShortestPath_Datastructuren;

import DijkstraShortestPath_Datastructuren.StepTypes.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

public class Visualiser implements KeyListener, MouseListener {
    JFrame frame;
    Painter painter;
    Set<Character> key_holder = new HashSet<>();
    ConnectionsMap connections_map;

    public Visualiser(int width, int height) {
        frame = new JFrame();
        painter = new Painter();
        frame.add(painter);
        frame.setSize(width, height);
        frame.setTitle("Dijkstra Visualization");
        frame.addKeyListener(this);
        frame.addMouseListener(this);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void setConnectionsMap(ConnectionsMap connections_map) {
        this.connections_map = connections_map;
        painter.set_map(connections_map);
    }

    public void run() {
        frame.setVisible(true);
        boolean running = true;
        while(running) {
            if(key_holder.contains('d')) {
                painter.step_type = Drive.class;
            } else if(key_holder.contains('f')) {
                painter.step_type = Flight.class;
            } else if(key_holder.contains('t')) {
                painter.step_type = TrainRide.class;
            } else if(key_holder.contains(' ')) {
                running = false;
            }

            painter.repaint();
            try {
                Thread.sleep(1000/20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        frame.dispose();
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        key_holder.add(keyEvent.getKeyChar());
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        key_holder.remove(keyEvent.getKeyChar());
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        System.out.println(mouseEvent.getX() +" "+ mouseEvent.getY());
        for(Node node : connections_map.get_all_nodes()) {
            if(node.mouse_on(mouseEvent.getX(), mouseEvent.getY())) {
                int button = mouseEvent.getButton();
                if(button == 1) {    //Left mouse button
                    painter.begin_id = node.get_id();
                } else if(button == 3) {    //Right mouse button
                    painter.end_id = node.get_id();
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}

class Painter extends JPanel {
    private ConnectionsMap map;
    //The following are also the defaults
    public int begin_id = 1;
    public int end_id = 5;
    public Class<?> step_type = Drive.class;

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        //Paints the path finding
        try {
            Route route = map.dijkstra(begin_id, end_id, step_type);
            for(Step step : route.get_steps()) {
                step.paint(g, map, Color.GREEN, 15);
            }
            g.setColor(Color.BLACK);
            g.drawString("Total: " + route.total_cost() +" "+ route.get_unit(), 4, 39);
        } catch (Exception e) {
            g.drawString("No route possible between node " + begin_id + " and " + end_id, 4, 39);
        }

        //Gives user information
        g.setColor(Color.BLACK);
        g.drawString("Traveling type selected: " + step_type.toString().split("\\.")[2], 4, 52);
        g.drawString("Use left and right mouse button to change the begin and end node", 4, 13);
        g.drawString("Use D, F and T to change traveling type", 4, 26);


        //Paint the steps
        List<Step> steps_done = new ArrayList<>();
        for(Node node : map.get_all_nodes()) {
            for(Step step : node.all_connections_in()) {
                if(!steps_done.contains(step)) {
                    step.paint_normal(g, map);
                    steps_done.add(step);
                }
            }
        }

        //Paint the nodes
        for(Node node: map.get_all_nodes()) {
            if(node.get_id() == begin_id) {
                node.paint(g, Color.ORANGE);
            } else if(node.get_id() == end_id) {
                node.paint(g, Color.CYAN);
            } else {
                node.paint_normal(g);
            }
        }
    }

    public void set_map(ConnectionsMap map) {
        this.map = map;
    }
}
