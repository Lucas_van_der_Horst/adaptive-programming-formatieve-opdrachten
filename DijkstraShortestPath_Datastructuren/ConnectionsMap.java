package DijkstraShortestPath_Datastructuren;

import java.util.*;

public class ConnectionsMap {
    private final HashMap<Integer, Node> nodes = new HashMap<>();

    public void add_node(Node node) {
        nodes.put(node.get_id(), node);
    }

    public Node get_node(int id) {
        return nodes.get(id);
    }

    public void add_connection(Step connection) {
        nodes.get(connection.get_begin_id()).add_connection(connection);
        nodes.get(connection.get_end_id()).add_connection(connection);
    }

    public void add_connection_both_ways(Step connection) {
        Step reversed = connection.reversed_clone();
        add_connection(connection);
        add_connection(reversed);
    }

    public <T> Route dijkstra(int begin_id, int end_id, Class<T> step_type) {
        //Source: https://youtu.be/GazC3A4OQTE

        PriorityQueue<Node> priority_queue = new PriorityQueue<>();
        List<Integer> done_ids = new ArrayList<>();
        Node begin_node = get_node(begin_id);
        begin_node.cost = 0;
        begin_node.previous_step = null;
        priority_queue.add(begin_node);

        while(true) {
            Node polled = priority_queue.poll();

            if(polled == null) {    //If there is nothing to explore, there is no path
                throw new IllegalArgumentException("No route possible.");
            } else if(polled.get_id() == end_id) { //Check if it is finished
                return route_by_backtracking_node(polled);
            } else {
                done_ids.add(polled.get_id());
                expand(polled, priority_queue, step_type, done_ids);
            }
        }
    }

    private <T> void expand(Node selected, PriorityQueue<Node> priority_queue, Class<T> step_type, List<Integer> ids_done) {
        for(Step step : selected.get_steps_of_type(step_type)) {
            Node new_node = get_node(step.get_end_id());
            double new_cost = selected.cost + step.get_cost();
            if((priority_queue.contains(new_node) && new_node.cost < new_cost) | ids_done.contains(new_node.get_id())) {
                continue;
            }
            new_node.cost = new_cost;
            new_node.previous_step = step;
            if(!priority_queue.contains(new_node)) {
                priority_queue.add(new_node);
            }
        }
    }

    private Route route_by_backtracking_node(Node node) {
        Route route = new Route();
        while(true) {
            Step previous_step = node.previous_step;
            if(previous_step == null) {
                break;
            }
            route.add_step_front(previous_step);
            node = get_node(previous_step.get_begin_id());
        }
        return route;
    }

    public List<Node> get_all_nodes() {
        return new ArrayList<>(nodes.values());
    }
}
