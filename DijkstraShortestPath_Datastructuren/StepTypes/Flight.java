package DijkstraShortestPath_Datastructuren.StepTypes;

import DijkstraShortestPath_Datastructuren.Step;

import java.awt.*;

public class Flight extends Step {
    private final double price;   //In euro's

    public Flight(int begin_id, int end_id, double price) {
        super(begin_id, end_id);
        this.price = price;
    }

    @Override
    public double get_cost() {
        return price;
    }

    @Override
    public String get_unit() {
        return "euros";
    }

    @Override
    public Step clone() {
        return new Flight(get_begin_id(), get_end_id(), get_cost());
    }

    @Override
    public Color get_color() {
        return Color.BLUE;
    }

    @Override
    public int paint_offset() {
        return 2;
    }
}
