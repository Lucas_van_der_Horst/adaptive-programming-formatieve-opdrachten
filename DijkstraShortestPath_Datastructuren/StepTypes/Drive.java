package DijkstraShortestPath_Datastructuren.StepTypes;

import DijkstraShortestPath_Datastructuren.Step;

import java.awt.*;

public class Drive extends Step {
    private final double distance;    //In kilometers

    public Drive(int begin_id, int end_id, double distance) {
        super(begin_id, end_id);
        this.distance = distance;
    }

    @Override
    public double get_cost() {
        return distance;
    }

    @Override
    public String get_unit() {
        return "km";
    }

    @Override
    public Step clone() {
        return new Drive(get_begin_id(), get_end_id(), get_cost());
    }

    @Override
    public Color get_color() {
        return Color.RED;
    }

    @Override
    public int paint_offset() {
        return -2;
    }
}
