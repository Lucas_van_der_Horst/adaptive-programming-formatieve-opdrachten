package DijkstraShortestPath_Datastructuren.StepTypes;

import DijkstraShortestPath_Datastructuren.Step;

import java.awt.*;

public class TrainRide extends Step {
    private final double time;    //In minutes

    public TrainRide(int begin_id, int end_id, double time) {
        super(begin_id, end_id);
        this.time = time;
    }

    @Override
    public double get_cost() {
        return time;
    }

    @Override
    public String get_unit() {
        return "minutes";
    }

    @Override
    public Step clone() {
        return new TrainRide(get_begin_id(), get_end_id(), get_cost());
    }

    @Override
    public Color get_color() {
        return Color.ORANGE;
    }

    @Override
    public int paint_offset() {
        return 4;
    }
}
