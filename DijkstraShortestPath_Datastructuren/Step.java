package DijkstraShortestPath_Datastructuren;

import java.awt.*;

public abstract class Step {
    private int begin_id;
    private int end_id;

    public Step(int begin_id, int end_id) {
        this.begin_id = begin_id;
        this.end_id = end_id;
    }

    public abstract double get_cost();

    public abstract String get_unit();

    public int get_begin_id() {
        return begin_id;
    }

    public int get_end_id() {
        return end_id;
    }

    public Step reversed_clone() {
        Step clone = clone();
        int temp = clone.begin_id;
        clone.begin_id = clone.end_id;
        clone.end_id = temp;
        return clone;
    }

    public abstract Step clone();

    public void paint(Graphics g, ConnectionsMap map, Color color, int thickness) {
        Node begin_node = map.get_node(get_begin_id());
        Node end_node = map.get_node(get_end_id());
        g.setColor(color);
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(thickness));
        g2.drawLine(begin_node.x+paint_offset(), begin_node.y, end_node.x+paint_offset(), end_node.y);
        //TODO add arrow indication
    }

    public void paint_normal(Graphics g, ConnectionsMap map)  {
        paint(g, map, get_color(), 2);
        Node begin_node = map.get_node(get_begin_id());
        Node end_node = map.get_node(get_end_id());
        int text_x = (begin_node.x + end_node.x) / 2;
        int text_y = (begin_node.y + end_node.y) / 2;
        g.drawString(""+get_cost(), text_x, text_y-7);
    }

    public abstract Color get_color();

    public abstract int paint_offset();
}
