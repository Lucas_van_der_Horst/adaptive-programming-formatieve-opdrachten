package CollectionsFramework;

import java.util.*;

public class Main {
    public static void main(String[] args){
        RandomGenerator generator = new RandomGenerator();
        Sorter sorter = new Sorter();
        List<String> test_data = generator.generate_letters(30);
        System.out.println(sorter.bubble_sort(test_data));
        System.out.println(test_data);
    }
}
