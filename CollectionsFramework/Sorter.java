package CollectionsFramework;

import java.util.*;

public class Sorter {

    public static List<String> bubble_sort(List<String> input_list){
        List<String> sort_list = new ArrayList<>(input_list);
        boolean sorted = false;
        while(!sorted){
            sorted = true;
            for(int i=1; i<sort_list.size(); i++){
                if(compare(sort_list.get(i-1), sort_list.get(i)) > 0){
                    String temp = sort_list.get(i-1);
                    sort_list.set(i-1, sort_list.get(i)); //set left to right
                    sort_list.set(i, temp);            //set right to left
                    sorted = false;
                }
            }
        }
        return sort_list;
    }

    private static int compare(String left, String right){
        return left.compareTo(right);
    }

    private static int compare(int left, int right){
        return left - right;
    }
}
