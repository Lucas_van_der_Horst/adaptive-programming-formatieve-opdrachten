package CollectionsFramework;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomGenerator {
    Random random = new Random();

    public List<String> generate_letters(int amount){
        String available_letters = "abcdefghijklmnopqrstuvwxyz";
        List<String> result = new ArrayList<String>();
        for(int i=0; i<amount; i++){
            int random_int = random.nextInt(available_letters.length());
            result.add(String.valueOf(available_letters.charAt(random_int)));
        }
        return result;
    }
}
