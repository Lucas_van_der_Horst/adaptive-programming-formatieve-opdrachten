package machineMetTekstinvoer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Node {
    private final String name;
    private final Map<String, Node> connected = new HashMap<>();

    public Node(String name){
        this.name = name;
    }

    public void add_connection(String pathname, Node node){
        this.connected.put(pathname, node);
    }

    public ArrayList<String> run(String input_text){
        if(input_text.length() == 0){
            // end of recursion
            // if the input_text is empty, return a arraylist with only the name of the node
            return new ArrayList<>() {{
                add(name);
            }};
        } else {
            String key = input_text.substring(0, 1);    // first letter in string
            String remainder = input_text.substring(1); // rest of the string

            if(connected.containsKey(key)){
                // if path exists, use recursion to run the connected node
                ArrayList<String> result = connected.get(key).run(remainder);
                // after the "sub-node" is done, add name to the front of the result
                result.add(0, name);
                return result;
            } else {
                throw new IllegalArgumentException(key + " is not a path of node " + name);
            }
        }
    }

    /*
    Sources:
    https://www.codebyamir.com/blog/how-to-use-a-map-in-java
    https://stackoverflow.com/questions/5618975/split-string-at-index
     */
}
