package machineMetTekstinvoer;

public class Main {
    public static void main(String[] args){
        Node s0 = new Node("s0");
        Node s1 = new Node("s1");
        Node s2 = new Node("s2");
        Node s3 = new Node("s3");

        s0.add_connection("A", s2);
        s0.add_connection("B", s1);

        s1.add_connection("A", s1);
        s1.add_connection("B", s2);

        s2.add_connection("B", s3);

        s3.add_connection("A", s3);
        s3.add_connection("B", s0);

        test_case(s0, "BAAB");
        test_case(s0, "BABA");
        test_case(s2, "BABABBBAB");
    }

    static void test_case(Node node, String input_text){
        System.out.print("Using input-text: " + input_text + ", results in ");
        try{
            System.out.println(node.run(input_text));
        } catch (IllegalArgumentException ex){
            System.out.println("an error: " + ex.getMessage());
        }
    }
}
