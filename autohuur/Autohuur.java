package autohuur;

import autohuur.Klant;

public class Autohuur {
    private int aantalDagen;
    private Klant huurder;
    private Auto gehuurdeAuto;

    Autohuur(){

    }

    public void setAantalDagen(int aD){
        this.aantalDagen = aD;
    }

    public void setGehuurdeAuto(Auto gA){
        this.gehuurdeAuto = gA;
    }

    public Auto getGehuurdeAuto(){
        return this.gehuurdeAuto;
    }

    public void setHuurder(Klant k){
        this.huurder = k;
    }

    public Klant getHuurder(){
        return this.huurder;
    }

    public double totaalPrijs(){
        return this.aantalDagen * this.gehuurdeAuto.getPrijsPerDag() * (1-(this.getHuurder().getKorting()/100));
    }

    @Override
    public String toString(){
        String output = "\n";
        Auto auto = this.getGehuurdeAuto();
        Klant klant = this.getHuurder();

        if(auto != null){
            output = output + "\tautotype: " + auto + "\n";
        }else{
            output = output + "\ter is geen auto bekend\n";
        }

        if(klant != null){
            output = output + "\top naam van: " + klant + "\n";
        }else{
            output = output + "\ter is geen huurder bekend\n";
        }

        output = output + "\taantal dagen: " + this.aantalDagen + " en dat kost ";
        if(auto != null){
            output = output + this.totaalPrijs() + "\n";
        }else{
            output = output + "0.0\n";
        }

        return output;
    }
}
